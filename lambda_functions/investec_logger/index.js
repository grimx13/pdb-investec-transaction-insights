// This function requires environment variables for:
// GOOGLE_SHEET_ID: Sheet ID
// GOOGLE_APPLICATION_CREDENTIALS: Path to service account credential.json

const {google} = require('googleapis');

async function sheetsWrite (values) {

    const auth = new google.auth.GoogleAuth({
        scopes: ['https://www.googleapis.com/auth/spreadsheets']
    });
    const authClient = await auth.getClient();
    const sheets = google.sheets({
        version: 'v4',
        auth: authClient,
    });

    const request = {
        // The ID of the spreadsheet to update.
        spreadsheetId: process.env.GOOGLE_SHEET_ID,

        // The A1 notation of the values to update.
        range: 'transactions!A1:J1',

        // How the input data should be interpreted.
        valueInputOption: 'RAW',

        resource: {
            "values": values
        },

        auth: authClient
    };

    try {
        const response = (await sheets.spreadsheets.values.append(request)).data;
        // TODO: Change code below to process the `response` object:
        console.log(JSON.stringify(response, null, 2));
    } catch (err) {
        console.error(err);
    }
}

exports.handler = async (event) => {
    const transaction = JSON.parse(event.Records[0].body);
    console.log(transaction);
    let values = [
        [
            transaction['dateTime'],
            transaction['accountNumber'],
            transaction['type'],
            transaction['reference'],
            transaction['merchant']['name'],
            transaction['merchant']['category']['name'],
            transaction['merchant']['city'],
            transaction['card']['id'],
            transaction['merchant']['country']['name'],
            transaction['centsAmount'],
        ],
    ];
    await sheetsWrite(values).catch(console.error);

    // TODO: Implement error checking and response based
    //       on the outcome instead
    const response = {
        statusCode: 200,
        body: JSON.stringify('Lambda Executed'),
    };
    return response;
};