import json
import boto3
import os

def qeue_message(message, reference):
    sqs = boto3.client('sqs')
    
    queue_url = os.environ['SQS_QUEUE_URL']
    
    # Send message to SQS queue
    response = sqs.send_message(
        QueueUrl=queue_url,
        DelaySeconds=0,
        MessageAttributes={
            'Ref': {
                'DataType': 'String',
                'StringValue': reference
            }
        },
        MessageBody=(message),
        MessageGroupId='transactions'
    )

    return response['MessageId']

def lambda_handler(event, context):
    
    event_json = json.dumps(event, indent=None, separators=(',', ':'))
    print(event_json)
    reference = event['reference']

    if reference == 'simulated':
        response = 'Received simulated event'
    else:
        try: 
            sqs_message_id = qeue_message(event_json, reference)
        except Exception as e:
            print(f'Error occured: {e}')
        response = f'Received transaction with reference: {reference} processed with message id: {sqs_message_id}'

    return {
        "statusCode": 200,
        "body": response
    }
