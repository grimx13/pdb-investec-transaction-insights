# Investec Transaction Insights #

The following is a cost effective serverless project powered by Investec Programmable banking and AWS managed services to log and categorize your transactions in an easy to interact with Google Sheet for near realtime analytics on your trasaction data.

### Status ###

This app is still work in progress, all components can be improved on and requires tests but below is their basic working status.

Components:

* investec [DONE]
* lambda_functions/investec_ingress [DONE]
* lambda_functions/investec_logger [TODO: ERROR HANDLING]
* terraform/ [TODO]

### Architecture ###

I went with serverless architecture as I believe the solution needs to be cheap to run and maintain and with something like transaction logging for individuals. 

The architecture is very modular and easy to expand on as I see new use cases for my financial life and as new features come to Investec. 

For Example:

The ingress Lambda function can be used to decide whether a transaction can be approved or declined as time is limited you would not want to add much more here, the SQS queue(s) is intended to move the transaction to other places where more timely execution can take place, which in turn can make data available (either via the Google Sheet or else where like DynamoDB or ElastiCache) for things like available budget to the ingress Lambda for decision making.

#### Traffic Flow ####

<img src = "images/investec_logger.png">

* A before method in Investec Programmable Banking posts the transcation as a JSON payload to the REST Amazon API Gateway.
* The API Gateway invokes the investec_ingress Lambda function checks if its non-simulated and sends the transaction to a SQS FIFO queue returning success with a SQS message id to Investec's function which is logged.
* The SQS queue invokes the investec_logger Lambda function which extracts the JSON transaction body parses useful fields and appends it to a Google Sheet which serves as the datastore and frontend.
* The Google sheet runs a SUMIF on certain useful categories I want to track my spend on and displays them in a graph for me to visualise.
  
### Deployment ###

Although there are plans to automate the creation of the AWS Infrastructure and assist with deployment of the Lambdas via Terraform, currently everything needs to be created manually.

Deploy Lambdas:

The root directory of each Lambda can be zipped and deployed via the AWS Console in Lambda.

### Insights ###

The versatility of Google Sheets is amazing to draw insights from once you have your data there, I have optimized my sheets for viewing on the go from my mobile. 

See some screenshots below on what I've come up with so far with very limited data, utilising the raw transaction log appended to from the `investec_logger` Lambda function:

<img src=images/sheets_screenshots.jpg>

The Geo Graph populated from the Mechant City info is unfortunately not suported by the Google Sheets Android app and so only viewable from Google Sheets web.

The raw transaction log is mutable and category overides along with other useful information is possible to append to it easily with drop down lists on the go from your phone.

### Costs ###

Since all of the architecture is managed pay as you use, and serverless, the solution is quite cheap considering individuals are unlikely to do 100s of transactions daily.

#### Lambda ####

```
Requests: $0.20 per 1M requests
Duration: $0.0000166667 for every GB-second

$0.0000002083 per 100ms of 128MB memory usage
```

#### API Gateway ####

```
$3.50 per 333 million requests (per month)
```

#### SQS FIFO ####

```
Data Transfered out:
Up to 1 GB / Month	$0.00 per GB
Next 9.999 TB / Month	$0.09 per GB
```
