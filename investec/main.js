// Call the API Gateway
// Requires environment variable for API_GATEWAY URI
async function callWebhook(transaction) {
    const response = await fetch(process.env.API_GATEWAY, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(transaction),
    });
  
    return response.json(Promise);
};

// Implemention of callWebhook in beforeTransaction
const beforeTransaction = async (authorization) => {
    response = await callWebhook(authorization);

    console.log(response);

    // TODO: Change based on response
    return true;
};